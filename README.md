# Portfolio project IDATA1003 - 2023
This file uses Mark Down syntax. For more information see [here](https://www.markdownguide.org/basic-syntax/).

STUDENT NAME = "Ylva Johanne Björnerstedt"  
STUDENT ID = "111679"

## Project description

This is a system that handles train departures departing from a station. 
Through the user menu, the user has the option to add and manage train departures.
The system also has a clock that has to be updated manually by the user, which will
then update the list of train departures. The user can also choose to display the train departures in the system.

## Project structure

This project was developed in Intellij using JDK 17.
The classes except test classes can be found in the src folder. 
All JUnit - test classes are stored in the test folder.

## Link to repository

https://gitlab.stud.idi.ntnu.no/ylvajb/idatt1003-2023-traindispatchsystem

## How to run the project

The main class is the TrainDispatchApp which creates an instance of the UserInterface
and calls on init() and start() in UserInterface. This will launch a user menu where the user has the option to exit the program,
in addition to other operations. The program will run until the user chooses to exit the program.
There already exist a few train departures in the system to test the program,
but the user can add and remove train departures as they wish.

## How to run the tests

The test classes can be found in the test folder and can be run by right-clicking on the test class and choose run.
There are 2 test classes in total, one for the TrainDeparture class and one for the TrainDepartureRegister class.
The test classes contain negative and positive tests, and the tests are sorted into nested tests depending on task.

## References
There are no references for this project.
