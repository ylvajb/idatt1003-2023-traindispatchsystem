package edu.ntnu.stud;

import java.time.Duration;
import java.time.LocalTime;

/**
 * This class represents a train departure.
 *
 * <p> A train departure consists of the following:
 * <ul><li>delay as Duration</li>
 * <li>departureTime as LocalTime</li>
 * <li>destination as a String</li>
 * <li>line as String</li>
 * <li>track as int</li>
 * <li>trainNumber as int</li></ul></p>
 *
 *  @author Ylva Johanne Björnerstedt
 *  @version 1.0
 *  @since 0.1
 */

public class TrainDeparture implements Comparable<TrainDeparture> {
  private Duration delay;
  private final LocalTime departureTime;
  private final String destination;
  private final String line;
  private int track;
  private final int trainNumber;

  /**
   * Validates the departure time.
   *
   * @param departureTime the departure time as a String on the format HH:mm
   *
   * @throws IllegalArgumentException if departureTime is null
   */

  private void validateDepartureTime(LocalTime departureTime) throws IllegalArgumentException {
    if (departureTime == null) {
      throw new IllegalArgumentException("Departure time cannot be null");
    }
  }
  /**
   * Validates the destination.
   *
   * @param destination the destination as a String
   *
   * @throws IllegalArgumentException if destination is null or blank
   */

  private void validateDestination(String destination) throws IllegalArgumentException {
    if (destination == null || destination.isEmpty()) {
      throw new IllegalArgumentException("Destination cannot be null");
    }
  }

  /**
   * Validates the train line.
   *
   * @param line the train line as a String
   *
   * @throws IllegalArgumentException if line is null or blank
   */
  private void validateLine(String line) throws IllegalArgumentException {
    if (line == null || line.isEmpty()) {
      throw new IllegalArgumentException("Line cannot be null");
    }
  }
  /**
   * Validates the track.
   *
   * @param track the track as an int
   *
   * @throws IllegalArgumentException if track is less than -1
   */

  private void validateTrack(int track) {
    if (track < -1) {
      throw new IllegalArgumentException("Invalid track");
    }
  }

  /**
   * Validates the train number.
   *
   * @param trainNumber the train number as an int
   *
   * @throws IllegalArgumentException if trainNumber is negative
   */
  private void validateTrainNumber(int trainNumber) throws IllegalArgumentException {
    if (trainNumber <= 0) {
      throw new IllegalArgumentException("Train number cannot be negative");
    }
  }

  /**
   * Constructor for TrainDeparture.
   *
   * @param departureTime the departure time as a String on the format HHmm
   * @param trainNumber   the train number as an int
   * @param line          the line as a String
   * @param destination   the destination as a String
   * @param track         the track as an int
   *
   * @throws IllegalArgumentException if:
   *                                  <ul> <li>departureTime is null</li>
   *                                  <li>trainNumber is negative</li>
   *                                  <li>line is null or blank</li>
   *                                  <li>destination is null or blank</li>
   *                                  <li>track is less than -1</li></ul>
   */
  public TrainDeparture(LocalTime departureTime, int trainNumber, String line,
                        String destination, int track) throws IllegalArgumentException {
    //Validating the input
    validateDepartureTime(departureTime);
    validateTrainNumber(trainNumber);
    validateLine(line);
    validateDestination(destination);
    validateTrack(track);
    if (track == 0) {
      track = -1;
    }
    //Setting the fields
    this.departureTime = departureTime;
    this.trainNumber = trainNumber;
    this.line = line;
    this.destination = destination;
    this.track = track;
    this.delay = Duration.ZERO;
  }
  /**
   * Constructor for TrainDeparture when track is not assigned,
   *        calls the other constructor with track set to -1.
   *
   * @param departureTime the departure time as a String on the format HHmm
   * @param trainNumber   the train number as an int
   * @param line          the line as a String
   * @param destination   the destination as a String
   *
   * @throws IllegalArgumentException if:
   *                                  <ul> <li>departureTime is null</li>
   *                                  <li>trainNumber is negative</li>
   *                                  <li>line is null or blank</li>
   *                                  <li>destination is null or blank</li>
   *                                  <li>track is less than -1</li></ul>
   */

  public TrainDeparture(LocalTime departureTime, int trainNumber, String line,
                        String destination) {
    this(departureTime, trainNumber, line, destination, -1);
  }

  /**
   * Gets the delay as an object of the class Duration.
   *
   * @return the delay as a Duration object
   */
  public Duration getDelay() {
    return delay;
  }

  /**
   * Gets the departure time as an object of the class LocalTime.
   *
   * @return the departure time as an object of LocalTime
   */
  public LocalTime getDepartureTime() {
    return departureTime;
  }

  /**
   * Gets the departure time plus the delay as an object of the class LocalTime.
   *
   * @return the departure time plus the delay as an object of LocalTime
   */
  public LocalTime getDepartureTimePlusDelay() {
    return departureTime.plus(delay);
  }

  /**
   * Gets the destination as a String.
   *
   * @return the destination as a String
   */
  public String getDestination() {
    return destination;
  }

  /**
   * Gets the line as a String.
   *
   * @return the line as a String
   */
  public String getLine() {
    return line;
  }

  /**
   * Gets the track as an int.
   *
   * @return the track as an int
   */
  public int getTrack() {
    return track;
  }

  /**
   * Gets the train number as an int.
   *
   * @return the train number as an int
   */

  public int getTrainNumber() {
    return trainNumber;
  }

  /**
   * Sets the delay to the given delay.
   *
   * @param delay the delay as a duration object
   *
   * @throws IllegalArgumentException if delay is null or negative
   */

  public void setDelay(Duration delay) throws IllegalArgumentException {
    if (delay == null || delay.isNegative()) {
      throw new IllegalArgumentException("Delay cannot be null or negative");
    }
    this.delay = delay;
  }

  /**
   * Sets the track to the given track. If track is not assigned(-1 or 0), the track is set to -1.
   *
   *
   * @param track the track as an int
   *
   * @throws IllegalArgumentException if track is less than -1
   */
  public void setTrack(int track) throws IllegalArgumentException {
    validateTrack(track);
    if (track == 0) {
      track = -1;
    }
    this.track = track;
  }

  /**
   * Formats the departure as a row in a table.
   *
   * @param formatter the formatter for the table, not including the delay and track
   * @param delaySpacing the delaySpacing as an int
   * @param trackSpacing the trackSpacing as an int
   *
   * @return the train departure as a String
   */
  public String tableFormat(String formatter, int delaySpacing, int trackSpacing) {
    LocalTime delayLocalTime = LocalTime.of(0, 0).plus(delay);
    String row = String.format(formatter, departureTime, line, trainNumber, destination);
    if (!delay.isZero()) {
      //If the train is delayed, the delay is printed
      row += String.format(" %-" + delaySpacing + "s |", delayLocalTime);
    } else {
      //If the train is not delayed, the cell is empty
      row += String.format(" %-" + delaySpacing + "s |", " ");
    }
    if (track != -1) {
      //If the train has a track, the track is printed
      row += String.format(" %-" + trackSpacing + "s |", track);
    } else {
      //If the train does not have a track, the cell is empty
      row += String.format(" %-" + trackSpacing + "s |", " ");
    }
    return row;
  }

  /**
   * Compares this train departure to another train departure.
   *
   * @param other the other train departure
   * @return 1 if this departure time is before the other departure time, -1 if this departure time
   *        is after the other departure time, and 0 if the departure times are equal
   */
  @Override
  public int compareTo(TrainDeparture other) {
    return this.departureTime.compareTo(other.departureTime);
  }
  /**
   * Compares this train departure to another object.
   *
   * @param other the other object
   * @return true if the other object is a train departure and all fields are equal, false
   *        otherwise
   */

  @Override
  public boolean equals(Object other) {
    if (other == null) {
      return false;
    }
    if (!(other instanceof TrainDeparture otherDeparture)) {
      return false;
    }
    return this.departureTime.equals(otherDeparture.departureTime)
        && this.line.equals(otherDeparture.line)
        && this.trainNumber == otherDeparture.trainNumber
        && this.destination.equals(otherDeparture.destination)
        && this.track == otherDeparture.track
        && this.delay.equals(otherDeparture.delay);
  }

  /**
   * Returns the train departure as a String.
   *
   * @return the train departure as a String
   */
  @Override
  public String toString() {
    return "Togavgang: \n"
        + "DepartureTime: " + departureTime + "\n"
        + "Line: " + line + "\n"
        + "TrainNumber: " + trainNumber + "\n"
        + "Destination: " + destination + "\n"
        + "Track: " + track + "\n"
        + "Delay: " + delay;
  }
}
