package edu.ntnu.stud;

import java.time.Duration;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Collections;

/**
 * This class represents a train departure. The class has the following attributes:
 * <ul><li>allDepartures as ArrayList</li>
 * <li>clock as LocalTime</li></ul>
 *
 *  @author Ylva Johanne Björnerstedt
 *  @version 1.0
 *  @since 0.1
 */
public class TrainDepartureRegister  {
  private final ArrayList<TrainDeparture> allDepartures;
  private LocalTime clock;

  /**
   * Constructor for TrainDepartureSystem.
   * Sets the clock to 00:00 and creates an ArrayList of all train departures.
   */
  public TrainDepartureRegister() {
    clock = LocalTime.of(0, 0);
    allDepartures = new ArrayList<>();
  }

  /**
   * Gets the list of all train departures.
   *
   * @return the list of all train departures as an ArrayList
   */
  public ArrayList<TrainDeparture> getAllDepartures() {
    return allDepartures;
  }

  /**
   * Gets a train departure with a given train number.
   *
   * @param trainNumber the train number of the train
   * @return the train as a TrainDeparture object
   *
   * @throws IllegalArgumentException if the train does not exist
   */
  public TrainDeparture getDeparture(int trainNumber) throws IllegalArgumentException {
    //Finding the train with the given train number
    for (TrainDeparture train : allDepartures) {
      if (train.getTrainNumber() == trainNumber) {
        return train;
      }
    }
    throw new IllegalArgumentException("Train with train number "
        + trainNumber + " does not exist");
  }

  /**
   * Creates a table of all train departures with a given destination as a String.
   *
   * @param destination the destination of the train departures to print
   * @return the table of the train departures as a String, or a message
   *        if no train departures with the given destination exists
   *
   * @throws IllegalArgumentException if the destination is null or blank
   */
  public ArrayList<TrainDeparture> getDeparturesToDestination(String destination)
      throws IllegalArgumentException {
    if (destination == null || destination.isBlank()) {
      throw new IllegalArgumentException("Destination cannot be null or blank");
    }
    destination = destination.toLowerCase();
    ArrayList<TrainDeparture> departures = new ArrayList<>();
    //Looking for train departures with the given destination
    for (TrainDeparture departure : allDepartures) {
      if (departure.getDestination().toLowerCase().equals(destination)) {
        departures.add(departure);
      }
    }
    if (departures.isEmpty()) {
      throw new IllegalArgumentException("No departures to " + destination + " where found");
    }
    return departures;
  }

  /**
   * Returns the total number of tracks available.
   *
   * @return the number of tracks as an int
   */
  public int getNumberOfTracks() {
    return 10;
  }

  /**
   * Gets the clock object.
   *
   * @return the clock object
   */

  public LocalTime getTime() {
    return clock;
  }

  /**
   * Sets delay to a train departure with a given train number.
   *
   * @param departure the train departure to set the delay of
   * @param delay the new delay of the train as a Duration object
   *
   * @throws IllegalArgumentException if:
   *                                 <ul><li>departure is null</li>
   *                                 <li>departure does not exist</li>
   *                                 <li>delay is null or negative</li>
   *                                 <li>track is in use at the given time</li></ul>
   */
  public void setDelay(TrainDeparture departure, Duration delay) throws IllegalArgumentException {
    if (departure == null) {
      throw new IllegalArgumentException("Departure cannot be null");
    } else if (!allDepartures.contains(departure)) {
      throw new IllegalArgumentException("Departure does not exist");
    } else if (delay == null || delay.isNegative()) {
      throw new IllegalArgumentException("Delay cannot be null or negative");
    } else if (checkTrack(departure.getTrack(), departure.getDepartureTime().plus(delay))) {
      throw new IllegalArgumentException("Track is in use at the given time");
    }
    departure.setDelay(delay);
  }

  /**
   * Sets the clock object.
   *
   * @param newTime the new clock object
   *
   * @throws IllegalArgumentException if the new time is null, before or equal to the current time
   */
  public void setTime(LocalTime newTime) throws IllegalArgumentException {
    if (newTime == null || newTime.isBefore(clock) || newTime.equals(clock)) {
      throw new IllegalArgumentException("New time has to be after the current time");
    }
    clock = newTime;
    updateTable();
  }
  /**
   * Assigns track to a departure.
   *
   * @param departure the train departure to set the delay of
   * @param track the new track of the train as an int
   *
   * @throws IllegalArgumentException if:
   *                                 <ul> <li>departure is null</li>
   *                                 <li>departure does not exist</li>
   *                                 <li>track is less than -1</li>
   *                                 <li>track is in use at the given time</li></ul>
   */

  public void setTrack(TrainDeparture departure, int track) throws IllegalArgumentException {
    if (departure == null) {
      throw new IllegalArgumentException("Departure cannot be null");
    } else if (!allDepartures.contains(departure)) {
      throw new IllegalArgumentException("Departure does not exist");
    } else if (track == 0) {
      track = -1;
    }
    if (track < -1) {
      throw new IllegalArgumentException("Track cannot be less than -1");
    } else if (checkTrack(track, departure.getDepartureTime())) {
      throw new IllegalArgumentException("Track is in use at the given time");
    }
    departure.setTrack(track);
  }

  /**
   * Checks if a train line is in use at a given time.
   *
   * @param line the line of the train as a String
   * @param departureTime the departure time of the train as a LocalTime object
   * @return true if the track is in use, false if the track is not in use
   */
  public boolean checkLine(String line, LocalTime departureTime, String destination) {
    return allDepartures.stream().anyMatch(train -> train.getLine().equals(line)
        && train.getDepartureTimePlusDelay().equals(departureTime)
        && train.getDestination().equals(destination));
  }

  /**
   * Checks if a track is in use at a given time.
   *
   * @param track the train track as an int
   * @param departureTime the departure time of the train as a LocalTime object
   * @return true if the track is in use, false if the track is not in use
   */

  public boolean checkTrack(int track, LocalTime departureTime) {
    return allDepartures.stream().anyMatch(train -> train.getTrack() == track
        && train.getDepartureTimePlusDelay().equals(departureTime));
  }

  /**
   * Checks if a train departure with a given train number exists.
   *
   * @param trainNumber the train number of the train
   * @return true if the train exists, false if the train does not exist
   */
  public boolean checkTrainNumber(int trainNumber) {
    return allDepartures.stream().anyMatch(train -> train.getTrainNumber() == trainNumber);
  }

  /**
   * Adds a train departure to the list of all train departures.
   *
   * <p>
   * If the departure is not null, will add the departure to the ArrayList of all departures.
   * The ArrayList of all departures is then sorted by departure time.
   * </p>
   *
   * @param time the departure time of the train as a LocalTime object
   * @param trainNumber the train number of the train as an int
   * @param line the line of the train as a String
   * @param destination the destination of the train as a String
   * @param track the track of the train as an int
   *
   * @throws IllegalArgumentException if:
   *                                  <ul><li>departureTime is null</li>
   *                                  <li>trainNumber is negative</li>
   *                                  <li>line is null or blank</li>
   *                                  <li>destination is null or blank</li>
   *                                  <li>track is less than -1</li></ul>
   */
  public void createDeparture(LocalTime time, int trainNumber, String line, String destination,
                           int track) throws IllegalArgumentException {
    //If cannot create departure, it will throw an IllegalArgumentException
    TrainDeparture departure = new TrainDeparture(time, trainNumber, line, destination, track);

    if (checkLine(line, time, destination)) {
      throw new IllegalArgumentException("Line is in use at the given time");
    } else if (checkTrainNumber(trainNumber)) {
      throw new IllegalArgumentException("Train number is in use");
    } else if (checkTrack(-1, time)) {
      throw new IllegalArgumentException("Track is in use at the given time");
    } else if (allDepartures.contains(departure)) {
      throw new IllegalArgumentException("Departure already exists");
    } else {
      allDepartures.add(departure);
      Collections.sort(allDepartures);
    }
  }

  /**
   * Adds a train departure to the list of all train departures without a track.
   *
   * <p>
   * If the departure is not null, will add the departure to the ArrayList of all departures.
   * The ArrayList of all departures is then sorted by departure time.
   * </p>
   *
   * @param time the departure time of the train as a LocalTime object
   * @param trainNumber the train number of the train as an int
   * @param line the line of the train as a String
   * @param destination the destination of the train as a String
   *
   * @throws IllegalArgumentException if:
   *                                  <ul> <li>could not create departure</li>
   *                                  <li>trainNumber is in use</li>
   *                                  <li>line is in use at the given time</li>
   *                                  <li>track is in use at the given time</li>
   *                                  <li>the departure already exists</li></ul>
   */
  public void createDeparture(LocalTime time, int trainNumber, String line,
                              String destination) throws IllegalArgumentException {
    TrainDeparture departure = new TrainDeparture(time, trainNumber, line, destination);
    if (checkLine(line, time, destination)) {
      throw new IllegalArgumentException("Line is in use at the given time");
    } else if (checkTrainNumber(trainNumber)) {
      throw new IllegalArgumentException("Train number is in use");
    } else if (allDepartures.contains(departure)) {
      throw new IllegalArgumentException("Departure already exists");
    } else {
      allDepartures.add(departure);
      Collections.sort(allDepartures);
    }
  }

  /**
   * Removes a train departure from the list of all train departures.
   *
   * @param departure the train departure to remove
   *
   * @throws IllegalArgumentException if the departure is null or does not exist
   */
  public void removeDeparture(TrainDeparture departure)
      throws IllegalArgumentException {
    if (departure == null) {
      throw new IllegalArgumentException("Departure cannot be null");
    } else if (!allDepartures.contains(departure)) {
      throw new IllegalArgumentException("Departure does not exist");
    }
    allDepartures.remove(departure);
  }

  /**
   * Updates the table of all train departures,
   * removing all train departures with a departure time past the current time.
   */

  public void updateTable() {
    allDepartures.removeIf(train -> train.getDepartureTime()
        .plusHours(train.getDelay().toHours()).minusMinutes(1)
        .isBefore(clock));
  }

  /**
   * Creates a table of a list of train departures as a String.
   *
   * @param departures the list of train departures to print
   * @return the table of the train departures as a String, or a message if the list is empty
   */
  public String createTable(ArrayList<TrainDeparture> departures) {
    //First checking if there are any train departures to print
    if (departures.isEmpty()) {
      return "No departures to print";
    }
    //Creating the formatter for the table
    int departureTimeSpacing = 12;
    int lineSpacing = 10;
    int trainNumberSpacing = 15;
    int destinationSpacing = 20;
    int delaySpacing = 15;
    int trackSpacing = 8;
    int columns = 6;
    String format = "| %-" + departureTimeSpacing + "s | %-" + lineSpacing + "." + lineSpacing
        + "s | %-" + trainNumberSpacing + "s | %-" + destinationSpacing + "."
        + destinationSpacing + "s |";
    String headerFormat = format + " %-" + delaySpacing + "s | %-" + trackSpacing + "s |";
    int rowLength = departureTimeSpacing + lineSpacing + trainNumberSpacing + destinationSpacing
        + delaySpacing + trackSpacing + columns * 3 + 1;
    StringBuilder departureString = new StringBuilder();

    //Creating the table of all train departures
    departureString.append("+").append(("-").repeat(rowLength - 2)).append("+").append("\n");
    departureString.append(String.format(headerFormat, "Avgangstid",
         "Linje", "Tognummer", "Destinasjon", "Forsinkelse", "Spor")).append("\n");
    for (TrainDeparture departure : departures) {
      //Creating rows for each train departure
      departureString.append(("-").repeat(rowLength)).append("\n");
      departureString.append(departure.tableFormat(
          format, delaySpacing, trackSpacing)).append("\n");
    }
    departureString.append("+").append(("-").repeat(rowLength - 2)).append("+").append("\n");
    return departureString.toString();
  }
}
