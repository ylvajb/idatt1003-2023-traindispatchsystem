package edu.ntnu.stud;

/**
 * This is the main class for the train dispatch application. It starts the program by calling the
 * init and start methods in the UserInterface class.
 *
 * @author Ylva Johanne Björnerstedt
 * @version 1.0
 * @since 0.1
 */
public class TrainDispatchApp {

  /**
   * Starts the program.
   *
   * @param args the command line arguments
   */

  public static void main(String[] args) {
    UserInterface menu = new UserInterface();
    menu.init();
    menu.start();

  }
}