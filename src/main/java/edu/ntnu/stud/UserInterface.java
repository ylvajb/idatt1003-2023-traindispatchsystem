package edu.ntnu.stud;

import java.time.Duration;
import java.time.LocalTime;
import java.util.ArrayList;

/**
 * <p>This class is responsible for the user menu and uses the
 * ValidInput class to validate user input. The class contains an instance
 * of the TrainDepartureRegister class, as well as the menu options
 * as Arrays of Strings, and the operations as constants.</p>
 *
 * @author Ylva Johanne Björnerstedt
 * @version 1,0
 * @since 0.1
 */

public class UserInterface {
  private TrainDepartureRegister station;
  private static final String[] MENU_MAIN = {"Print all train departures",
      "Add new train departure", "Find train departure with train number",
      "Search for train departure with destination", "Update clock", "Exit program"};
  private static final String[] MENU_EDIT_DEPARTURE = {"Print departure", "Assign track to train",
      "Add delay to train departure", "Remove departure", "Return to main menu"};

  //Operations for main menu
  private static final int PRINT_ALL_DEPARTURES = 1;
  private static final int ADD_DEPARTURE = 2;
  private static final int FIND_DEPARTURE_BY_TRAIN_NUMBER = 3;
  private static final int FIND_DEPARTURES_TO_DESTINATION = 4;
  private static final int UPDATE_CLOCK = 5;
  private static final int EXIT_PROGRAM = 6;

  //Operations for editing a departure
  private static final int PRINT_DEPARTURE = 1;
  private static final int ASSIGN_TRACK = 2;
  private static final int ADD_DELAY = 3;
  private static final int REMOVE_DEPARTURE = 4;
  private static final int RETURN_TO_MAIN_MENU = 5;

  /**
   * Adds a new departure to the register, asks the user for information to create a new deparure.
   */
  private void addDeparture() {
    //The user is asked to enter the departure time,
    //and the time is checked to be after the current time
    LocalTime currentTime = station.getTime();
    System.out.println("Enter departure time on the format HH:mm : ");
    LocalTime time = ValidInput.validTime();
    while (!time.isAfter(currentTime)) {
      System.out.println("Departure time has to be after "
          + currentTime + ". Write a new departure time: ");
      time = ValidInput.validTime();
    }
    //The user is asked to enter the train number, and the train number is checked to be unique
    System.out.println("Enter train number: ");
    int trainNumber = ValidInput.validPositiveInt();
    while (station.checkTrainNumber(trainNumber)) {
      System.out.println(trainNumber + " is already in use. Write a new train number: ");
      trainNumber = ValidInput.validPositiveInt();
    }
    System.out.println("Enter destination: ");
    String destination = ValidInput.validString();

    System.out.println("Enter train line: ");
    String line = ValidInput.validString();
    while (station.checkLine(line, time, destination)) {
      //Make sure the line is not in use at the given time
      System.out.println(line + " is already in use. Write a new line: ");
      line = ValidInput.validString();
    }
    String text;
    int track;
    do {
      System.out.println("Do you want to assign a track? [y/n]");
      text = ValidInput.validString().toLowerCase();
    } while (!text.equals("y") && !text.equals("n"));
    if (text.equals("y")) {
      //Assign track
      System.out.println("Enter track:");
      track = ValidInput.validIntRange(1, station.getNumberOfTracks() + 1);
      while (station.checkTrack(track, time)) {
        //Make sure the track is not in use at the given time
        System.out.println("Track " + track + " is already in use. Please enter a new track: ");
        track = ValidInput.validIntRange(1, station.getNumberOfTracks() + 1);
      }
      try {
        //When track is assigned
        station.createDeparture(time, trainNumber, line, destination, track);
      } catch (IllegalArgumentException e) {
        //In case of invalid input, ask user if they want to try again
        System.out.println("Could not create departure: " + e.getMessage());
        if (redo()) {
          addDeparture();
        }
      }
    } else {
      //When no track is assigned
      try {
        station.createDeparture(time, trainNumber, line, destination);
      } catch (IllegalArgumentException e) {
        //In case of invalid input, ask user if they want to try again
        System.out.println(e.getMessage());
        if (redo()) {
          addDeparture();
        }
      }
    }
  }

  /**
   * Assigns a track to the given departure.
   *
   * @param departure the departure to be assigned a track
   */
  private void assignTrack(TrainDeparture departure) {
    System.out.println("Assign track. Enter -1 or 0 if no track is assigned: ");
    int track = ValidInput.validIntRange(-1, station.getNumberOfTracks() + 1);
    if (track > 0 && station.checkTrack(track, departure.getDepartureTime())) {
      //If the track is in use at departure time, ask user if they want to try again
      System.out.println("Track " + track + " is already in use.");
      if (redo()) {
        assignTrack(departure);
      } else {
        System.out.println("Returning to menu...\n");
        return;
      }
    }
    try {
      station.setTrack(departure, track);
      printDeparture(departure);
    } catch (IllegalArgumentException e) {
      //In case of invalid input, ask user if they want to try again
      System.out.println(e.getMessage());
      if (redo()) {
        assignTrack(departure);
      }
    }
  }

  /**
   * Submenu for editing a departure.
   */
  private void editDepartureMenu() {
    //Ask user which departure to edit
    TrainDeparture departure = findDeparture();
    //IF the train does not exist, the user is returned to the main menu
    if (departure == null) {
      System.out.println("Returning to main menu...\n");
      return;
    }
    //Run menu
    boolean finished = false;
    while (!finished) {
      int choice = printMenu(MENU_EDIT_DEPARTURE);
      switch (choice) {
        case PRINT_DEPARTURE -> printDeparture(departure);

        case ASSIGN_TRACK -> assignTrack(departure);

        case ADD_DELAY -> setDelay(departure);

        case REMOVE_DEPARTURE -> {
          removeDeparture(departure);
          finished = true;
        }

        case RETURN_TO_MAIN_MENU -> finished = true;

        default -> System.out.println("Invalid choice."
            + " Your answer must be one of the options mentioned above.");
      }
    }
  }

  /**
   * Finds a departure with train number.
   *
   * @return the departure with the given train number
   */
  private TrainDeparture findDeparture() {
    System.out.println("Enter train number: ");
    int trainNumber = ValidInput.validPositiveInt();
    if (station.checkTrainNumber(trainNumber)) {
      //If the train exists, the user is asked to choose an operation
      return station.getDeparture(trainNumber);
    } else {
      //If departure is not found, ask user if they want to redo
      System.out.println("Train number " + trainNumber + " is not in use");
      if (redo()) {
        return findDeparture();
      } else {
        //If not redo, return to main menu
        return null;
      }
    }
  }

  /**
   * Prints the given departure.
   *
   * @param departure the departure to be printed
   */
  private void printDeparture(TrainDeparture departure) {
    ArrayList<TrainDeparture> train = new ArrayList<>();
    train.add(departure);
    System.out.println(station.createTable(train));
  }

  /**
   * Prints all departures to a destination. If no departures are found,
   * a message is printed.
   */
  private void printDeparturesToDestination() {
    System.out.println("Enter a destination: ");
    String destination = ValidInput.validString();
    try {
      System.out.println(station.createTable(station.getDeparturesToDestination(destination)));
    } catch (IllegalArgumentException e) {
      System.out.println(e.getMessage());
    }
  }

  /**
   * Prints a menu with the given options and asks the user to choose an option.
   *
   * @param menu the menu options as an array of Strings
   * @return the number of the chosen option
   */
  private int printMenu(String[] menu) {
    if (menu.length == 0) {
      return -1;
    }
    for (int i = 0; i < menu.length; i++) {
      System.out.println("[" + (i + 1) + "] " + menu[i]);
    }
    System.out.println("Enter an option: ");
    int choice = ValidInput.validIntRange(1, menu.length + 1);
    System.out.print("You chose: " + menu[choice - 1] + "\n");
    return choice;
  }

  /**
   * Asks the user if they want to redo operation after failure.
   *
   * @return the answer as a boolean
   */
  private boolean redo() {
    System.out.println("Do you want to try again? [y/n]");
    String text = ValidInput.validString().toLowerCase();
    return text.equals("y");
  }

  /**
   * Removes the departure given.
   *
   * @param departure the departure to be removed
   */
  private void removeDeparture(TrainDeparture departure) {
    System.out.println("Are you sure you want to remove this departure? [y/n]");
    String text = ValidInput.validString().toLowerCase();
    if (text.equals("y")) {
      try {
        station.removeDeparture(departure);
      } catch (IllegalArgumentException e) {
        System.out.println("Could not remove departure: " + e.getMessage());
      }
    }
  }

  /**
   * Adds delay to the departure given.
   *
   * @param departure the departure to be delayed
   */
  private void setDelay(TrainDeparture departure) {
    System.out.println("Enter delay on the format HH:mm :");
    Duration duration = Duration.between(LocalTime.MIN, ValidInput.validTime());
    //The duration is set by replacing the current delay
    try {
      station.setDelay(departure, duration);
      printDeparture(departure);
    } catch (IllegalArgumentException e) {
      System.out.println(e.getMessage());
      if (redo()) {
        setDelay(departure);
      }
    }
  }

  /**
   * Updates the clock by asking the user to enter a new time.
   * If the new time is before the current time, the user is asked to enter a new time.
   */
  private void updateClock() {
    LocalTime oldTime = station.getTime();
    System.out.println("Enter the new time on the format HH:mm :");
    LocalTime time = ValidInput.validTime();
    while (time.isBefore(oldTime) || time.equals(oldTime)) {
      System.out.println("Departure time has to be after "
          + oldTime + ". Please enter a new departure time: ");
      time = ValidInput.validTime();
    }
    try {
      station.setTime(time);
    } catch (IllegalArgumentException e) {
      //In case of invalid input, ask user if they want to try again
      System.out.println(e.getMessage());
      if (redo()) {
        updateClock();
      }
    }
  }

  /**
   * Initializes the program.
   */
  public void init() {
    station = new TrainDepartureRegister();
  }

  /**
   * Starts the program.
   */
  public void start() {

    station.createDeparture(LocalTime.parse("12:00"), 1, "F1", "Oslo", 1);
    station.createDeparture(LocalTime.parse("10:45"), 2, "F2", "Bergen", 4);
    station.createDeparture(LocalTime.parse("11:00"), 3, "R2", "Bergen", 5);
    station.createDeparture(LocalTime.parse("11:20"), 4, "R2", "Bergen", 5);

    int choice;
    System.out.println(("_").repeat(100));
    while (true) {
      System.out.println("The time is " + station.getTime());
      choice = printMenu(MENU_MAIN);
      switch (choice) {
        case PRINT_ALL_DEPARTURES ->
            System.out.println(station.createTable(station.getAllDepartures()));

        case ADD_DEPARTURE -> addDeparture();

        case FIND_DEPARTURE_BY_TRAIN_NUMBER -> editDepartureMenu();

        case FIND_DEPARTURES_TO_DESTINATION -> printDeparturesToDestination();

        case UPDATE_CLOCK -> updateClock();

        case EXIT_PROGRAM -> {
          return;
        }
        default -> System.out.println("Invalid choice."
            + " Your answer must be one of the options mentioned above");
      }
      System.out.println(("_").repeat(100));
    }
  }
}
