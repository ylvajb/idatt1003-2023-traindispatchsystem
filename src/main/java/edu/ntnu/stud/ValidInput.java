package edu.ntnu.stud;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

/**
 * <p>This class contains methods for validating user input. It is used by the UserInterface class.
 * The constructor is private and the methods are static, so the class cannot be instantiated.</p>
 * The fields are:
 * <ul><li>SCANNER as Scanner</li></ul>
 *
 *
 *  @author Ylva Johanne Björnerstedt
 *  @version 1.0
 *  @since 0.1
 */
public class ValidInput {
  private static final Scanner SCANNER = new Scanner(System.in);

  /**
   * Private constructor to prevent instantiation.
   */
  private ValidInput() {
  }

  /**
   * Asks the user to enter an integer between lowerRange and upperRange.
   *
   * @param lowerRange the lower range of the interval, inclusive
   * @param upperRange the upper range of the interval, non-inclusive
   * @return the number entered by the user as an int
   */
  public static int validIntRange(int lowerRange, int upperRange) {
    //If the lower range is greater than the upper range, the two are swapped
    if (lowerRange > upperRange) {
      int temp = lowerRange;
      lowerRange = upperRange;
      upperRange = temp;
    }
    int numb;
    while (true) {
      try {
        numb = SCANNER.nextInt();
        //Check if number is within range
        if (numb >= lowerRange && numb < upperRange) {
          SCANNER.nextLine();
          return numb;
        }
      } catch (Exception e) {
        SCANNER.next();
      }
      System.out.println("Invalid int. Write an int from " + lowerRange + " to " + upperRange);
    }
  }

  /**
   * Asks the user to enter an integer >= 0.
   *
   * @return the number entered by the user as an int >= 0
   */
  public static int validPositiveInt() {
    int numb;
    while (true) {
      try {
        numb = SCANNER.nextInt();
        if (numb >= 0) {
          SCANNER.nextLine();
          return numb;
        }
      } catch (Exception e) {
        SCANNER.next();
      }
      System.out.println("Invalid int. Write a positive int");
    }
  }

  /**
   * Asks the user to enter a String.
   *
   * @return the String entered by the user
   */
  public static String validString() {
    return SCANNER.nextLine();
  }

  /**
   * Asks the user to enter a valid time as a String on the format HHMM.
   *
   * @return the time entered by the user as a LocalTime object
   */
  public static LocalTime validTime() {
    final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm");
    LocalTime time;
    String timeString;
    while (true) {
      timeString = validString();
      try {
        time = LocalTime.parse(timeString, formatter);
        return time;
      } catch (Exception e) {
        System.out.println("Invalid time. Use the format HH:mm, remember to use colon");
      }
    }
  }
}
