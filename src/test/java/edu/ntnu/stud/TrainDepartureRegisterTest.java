package edu.ntnu.stud;

import java.time.LocalTime;
import java.util.ArrayList;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

/**
 * This class contains tests for the TrainDepartureRegister class.
 *
 * @author Ylva Johanne Björnerstedt
 * @version 1.0
 * @since 0.1
 */

public class TrainDepartureRegisterTest {
  private TrainDepartureRegister station;

  /**
   * This method is run before each test in this class and creates a new TrainDepartureRegister.
   */
  @BeforeEach
  public void setUp() {
    station = new TrainDepartureRegister();
  }

  /**
   * This is a nested class containing classes relating to setting and getting time.
   */
  @Nested
  @DisplayName("Tests related to setting and getting time")
  public class TimeTests {

    /**
     * Tests that the time is set to 00:00 when the TrainDepartureRegister object is created.
     */
    @Test
    @DisplayName("Time should be 00:00 when the object is created")
    public void shouldGetTimeAsMidnightWhenTheObjectIsInitialized() {
      TrainDepartureRegister station = new TrainDepartureRegister();
      Assertions.assertEquals(LocalTime.parse("00:00"), station.getTime());
    }

    /**
     * Tests that the time is set to 12:00 when parameter is 12:00.
     */
    @Test
    @DisplayName("Testing setTime:"
        + " should set time when time is not after current time and not null")
    public void shouldSetTimeWhenTimeIsAfterCurrentTimeAndNotNull() {
      station.setTime(LocalTime.parse("12:00"));
      Assertions.assertEquals(LocalTime.parse("12:00"), station.getTime());
    }

    /**
     * Tests that the time should not be set if the new time is null.
     */
    @Test
    @DisplayName("Testing setTime: should not set time if the new time is null")
    public void shouldThrowIllegalArgumentExceptionWhenTimeIsNull() {
      Assertions.assertThrows(IllegalArgumentException.class, () ->
          station.setTime(null));
    }

    /**
     * This is a parameterized test that tests if the given time is not after current time.
     *
     * @param time is the time that is tested.
     */
    @DisplayName("Testing setTime:"
        + " should not set time if the new time is before or equal to current time")
    @ParameterizedTest
    @ValueSource(strings = {"11:30", "12:00", "00:00"})
    public void shouldThrowIllegalArgumentExceptionWhenTimeIsNotAfterCurrentTime(String time) {
      TrainDepartureRegister station = new TrainDepartureRegister();
      station.setTime(LocalTime.parse("12:00"));
      Assertions.assertThrows(IllegalArgumentException.class, () ->
          station.setTime(LocalTime.parse(time)));
    }

    /**
     * Tests that getTime returns the time that is set.
     */
    @Test
    @DisplayName("Should get time when called")
    public void shouldGetTimeWhenCalled() {
      station.setTime(LocalTime.parse("12:00"));
      Assertions.assertEquals(LocalTime.parse("12:00"), station.getTime());
    }
  }

  /**
   * This is a nested class containing classes relating to adding departures to the register.
   */
  @Nested
  @DisplayName("Tests related to adding trains to system")
  public class CreateDepartureTests {

    /**
     * This method is run before each test in this class and adds a departure to the system.
     */
    @BeforeEach
    public void setUp() {
      station.createDeparture(LocalTime.parse("12:00"),
          23, "F1", "Oslo", 1);
    }

    /**
     * Tests that the departure is added to the register if all parameters are valid.
     */
    @Test
    @DisplayName("Should add train if everything is valid")
    public void shouldCreateDeparture() {
      Assertions.assertEquals(1, station.getAllDepartures().size());
    }

    /**
     * Tests that the departure is added to the register if all parameters
     * are valid and no track is given.
     */
    @Test
    @DisplayName("Should add train if track is not a given parameter")
    public void shouldCreateDepartureWhenNoTrackParameterIsGiven() {
      station.createDeparture(LocalTime.parse("09:00"),
          44, "F1", "Oslo");
      Assertions.assertEquals(2, station.getAllDepartures().size());
    }

    /**
     * Tests that departure is not added if the departure already exists in the register.
     */
    @Test
    @DisplayName("Should not add train if the train already exists")
    public void shouldThrowIllegalArgumentExceptionWhenTrainAlreadyExists() {
      Assertions.assertThrows(IllegalArgumentException.class, () ->
          station.createDeparture(LocalTime.parse("12:00"),
              23, "F1", "Oslo", 1));
    }
  }

  /**
   * This is a nested class containing classes relating to getting departures from the register.
   */
  @Nested
  @DisplayName("Tests related to getting trains from system")
  public class GetTrainTests {

    /**
     * Tests that getTrain returns the train with the given train number when departure exists.
     */
    @Test
    @DisplayName("Should get train when train number exists")
    public void shouldGetTrainWhenTrainNumberExists() {
      station.createDeparture(LocalTime.parse("12:00"),
          23, "F1", "Oslo", 1);
      Assertions.assertEquals(23, station.getDeparture(23).getTrainNumber());
    }

    /**
     * Tests that getTrain throws IllegalArgumentException when train number is not in the register.
     */
    @Test
    @DisplayName("Should not get train when train number is not in use")
    public void shouldThrowIllegalArgumentExceptionIfTrainIsNotFound() {
      Assertions.assertThrows(IllegalArgumentException.class, () ->
          station.getDeparture(200));
    }

    /**
     * Tests that getTrain returns an empty ArrayList when there are no departures in the register.
     */
    @Test
    @DisplayName("Should get empty list if the list is empty")
    public void shouldGetAnEmptyListOfAllDeparturesWhenThereAreNoDepartures() {
      Assertions.assertEquals(0, station.getAllDepartures().size());
    }

    /**
     * Tests that getAllDepartures returns a list with all departures in the register.
     */
    @Test
    @DisplayName("Should get list with departures of all the trains in the system")
    public void shouldGetAllDeparturesWithTwoDeparturesWhenThereAreToDeparturesInSystem() {
      TrainDepartureRegister station = new TrainDepartureRegister();
      station.createDeparture(LocalTime.parse("12:00"),
          23, "F2", "Oslo", 1);
      station.createDeparture(LocalTime.parse("12:00"),
          25, "F1", "Oslo", 1);
      Assertions.assertEquals(2, station.getAllDepartures().size());
    }
  }

  /**
   * This is a nested class containing classes relating to the information table.
   */
  @Nested
  @DisplayName("Tests related to the information table")
  public class InformationTableTests {

    /**
     * Tests that trains are removed from the register when departure time has passed.
     */
    @Test
    @DisplayName("Should remove train when departure time has passed")
    public void shouldRemoveTrainWhenDepartureTimeHasPassed() {
      station.createDeparture(LocalTime.parse("12:00"),
          23, "F1", "Oslo", 1);
      station.createDeparture(LocalTime.parse("13:00"),
          25, "F1", "Oslo", 1);
      station.setTime(LocalTime.parse("12:01"));
      station.updateTable();
      Assertions.assertEquals(1, station.getAllDepartures().size());
    }

    /**
     * Tests that the ArrayList with departures is sorted
     * by departure time after adding a departure.
     */
    @Test
    @DisplayName("Sort the departures by departure time after adding a departure")
    public void shouldSortDeparturesByTimeWhenNewDepartureIsAdded() {
      TrainDepartureRegister station = new TrainDepartureRegister();
      station.createDeparture(LocalTime.parse("12:00"),
          23, "F1", "Oslo", 1);
      station.createDeparture(LocalTime.parse("13:00"),
          25, "F1", "Oslo", 1);
      station.createDeparture(LocalTime.parse("11:00"),
          24, "F1", "Oslo", 1);
      Assertions.assertEquals(24, station.getAllDepartures().get(0).getTrainNumber());
      Assertions.assertEquals(23, station.getAllDepartures().get(1).getTrainNumber());
      Assertions.assertEquals(25, station.getAllDepartures().get(2).getTrainNumber());
    }

    /**
     * Tests that the createTable method returns an error message when the given list is empty.
     */
    @Test
    @DisplayName("Should return error message when the given list to print is empty")
    public void shouldReturnErrorMessageWhenCreatingTableListIsEmpty() {
      TrainDepartureRegister station = new TrainDepartureRegister();
      ArrayList<TrainDeparture> list = new ArrayList<>();
      Assertions.assertEquals("No departures to print", station.createTable(list));
    }
  }

  /**
   * This is a nested class containing classes relating to removing departures from the register.
   */
  @Nested
  @DisplayName("Tests related to removing departures in system")
  public class RemoveDepartureTests {

    /**
     * Tests that the departure is removed from the register
     * when the departure exists in the register.
     */
    @Test
    @DisplayName("Should remove departure")
    public void shouldRemoveDeparture() {
      station.createDeparture(LocalTime.parse("12:00"),
          23, "F1", "Oslo", 1);
      station.removeDeparture(station.getDeparture(23));
      Assertions.assertEquals(0, station.getAllDepartures().size());
    }

    /**
     * Tests that removeDeparture throws IllegalArgumentException when the
     * given train is null.
     */
    @Test
    @DisplayName("Should not remove departure if train is null")
    public void shouldThrowIllegalArgumentExceptionIfTrainIsNull() {
      Assertions.assertThrows(IllegalArgumentException.class, () ->
          station.removeDeparture(null));
    }

    /**
     * Tests that removeDeparture throws IllegalArgumentException when the
     * given train is not in the register.
     */
    @Test
    @DisplayName("Should not remove train if train is not in system")
    public void shouldThrowIllegalArgumentExceptionIfTrainIsNotInSystem() {
      Assertions.assertThrows(IllegalArgumentException.class, () ->
          station.removeDeparture(new TrainDeparture(LocalTime.parse("12:00"),
              23, "F1", "Oslo", 1)));
    }
  }

  /**
   * This is a nested class containing classes relating to methods cheking
   * if train number, line and track is in use.
   */
  @Nested
  @DisplayName("Tests related to cheking train number, line and track")
  public class CheckMethodsTests {
    /**
     * This method is run before each test in this class and adds departures to the system.
     */
    @BeforeEach
    public void setUp() {
      station.createDeparture(LocalTime.parse("12:00"),
          23, "F1", "Oslo", 1);
      station.createDeparture(LocalTime.parse("13:00"),
          12, "F1", "Oslo", 2);
      station.createDeparture(LocalTime.parse("10:00"),
          100, "R2", "Bergen", 5);
    }

    /**
     * Parameterized test that tests if the given train number is in use.
     *
     * @param trainNumber is the train number that is tested.
     */
    @ParameterizedTest
    @ValueSource(ints = {23, 12, 100})
    @DisplayName("Should return true when the train number is in use")
    public void shouldReturnTrueIfTrainNumberIsInUse(int trainNumber) {
      Assertions.assertTrue(station.checkTrainNumber(trainNumber));
    }

    /**
     * Parameterized test that tests if the given train number is not in use.
     *
     * @param trainNumber is the train number that is tested.
     */
    @DisplayName("Should return false when the train number is not in use")
    @ParameterizedTest
    @ValueSource(ints = {24, 2, 200})
    public void shouldReturnFalseIfTrainNumberIsNotUse(int trainNumber) {
      Assertions.assertFalse(station.checkTrainNumber(trainNumber));
    }

    /**
     * Tests if checkLine returns true if the line is in use.
     */
    @Test
    @DisplayName("Should return true if line is in use")
    public void shouldReturnTrueIfLineIsInUse() {
      Assertions.assertTrue(station.checkLine("F1", LocalTime.parse("12:00"), "Oslo"));
    }

    /**
     * Tests if checkLine returns false if the line is not in use.
     */
    @Test
    @DisplayName("Should return false if line is not in use")
    public void shouldReturnFalseIfLineIsNotInUse() {
      Assertions.assertFalse(station.checkLine("F1", LocalTime.parse("15:00"), "Oslo"));
    }

    /**
     * Tests if checkTrack returns true if the track is in use.
     */
    @Test
    @DisplayName("Should return true if track is in use")
    public void shouldReturnTrueIfTrackIsInUse() {
      Assertions.assertTrue(station.checkTrack(1, LocalTime.parse("12:00")));
    }

    /**
     * Tests if checkTrack returns false if the track is not in use.
     */
    @Test
    @DisplayName("Should return false if track is not in use")
    public void shouldReturnFalseIfTrackIsNotInUse() {
      Assertions.assertFalse(station.checkTrack(2, LocalTime.parse("12:00")));
    }
  }

  /**
   * This is a nested class containing classes relating to getting
   * departures to destination from the register.
   */
  @Nested
  public class TestGetDeparturesToDestination {

    /**
     * This method is run before each test in this class and adds departures to the system.
     */
    @BeforeEach
    public void setUp() {
      station.createDeparture(LocalTime.parse("12:00"),
          23, "F1", "Oslo", 1);
      station.createDeparture(LocalTime.parse("13:00"),
          25, "F1", "Oslo", 1);
      station.createDeparture(LocalTime.parse("11:00"),
          24, "F1", "Trondheim", 1);
    }

    /**
     * Tests that getDeparturesToDestination returns a list with all departures to destination.
     */
    @Test
    @DisplayName("Should get a list"
        + " of all trains to destination when there are trains to detstination")
    public void shouldGetAllDeparturesToDestinationWhenThereAreDeparturesToDestination() {
      Assertions.assertEquals(2, station.getDeparturesToDestination("Oslo").size());
    }

    /**
     * Tests that getDeparturesToDestination returns an empty list when there are no departures
     * to destination.
     */
    @Test
    @DisplayName("Should not get a list when no departures to destination are found")
    public void shouldThrowIllegalArgumentExceptionWhenNoDeparturesToDestinationAreFound() {
      Assertions.assertThrows(IllegalArgumentException.class, () ->
          station.getDeparturesToDestination("Bergen"));
    }

    /**
     * Tests that getDeparturesToDestination throws IllegalArgumentException when the
     * given destination is null.
     */
    @Test
    @DisplayName("Should not get a list when destination is null")
    public void shouldThrowIllegalArgumentExceptionIfDestinationIsNull() {
      Assertions.assertThrows(IllegalArgumentException.class, () ->
          station.getDeparturesToDestination(null));
    }
  }


}
