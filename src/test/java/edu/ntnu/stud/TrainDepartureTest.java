package edu.ntnu.stud;

import java.time.Duration;
import java.time.LocalTime;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;


/**
 * This class contains tests for the TrainDepartureRegister class.
 *
 * @author Ylva Johanne Björnerstedt
 * @version 1.0
 * @since 0.1
 */

public class TrainDepartureTest {
  private TrainDeparture departure;

  /**
   * This method rund before each test and creates a new TrainDeparture object.
   */
  @BeforeEach
  @DisplayName("Creating departure object")
  public void setUp() {
    departure = new TrainDeparture(
        LocalTime.parse("12:00"), 200, "F1", "Oslo", 1);
  }

  /**
   * Nested class that contains positive test related to the constructor.
   */
  @Nested
  @DisplayName("Positive tests for constructor")
  public class PositiveConstructorTest {

    /**
     * Tests that the constructor creates a TrainDeparture
     * object with the correct departure time.
     */
    @Test
    @DisplayName("Departure time should be 12:00 and an object of LocalTime")
    public void shouldGetDepartureTimeWhenCalled() {
      Assertions.assertEquals(LocalTime.parse("12:00"), departure.getDepartureTime());
    }

    /**
     * Tests that the constructor creates a TrainDeparture
     * object with the given line.
     */
    @Test
    @DisplayName("Should get line when called")
    public void shouldGetLineWhenCalled() {
      Assertions.assertEquals("F1", departure.getLine());
    }

    /**
     * Tests that the constructor creates a TrainDeparture
     * object with the given train number.
     */
    @Test
    @DisplayName("Should get train number when called")
    public void shouldGetTrainNumberWhenCalled() {
      Assertions.assertEquals(200, departure.getTrainNumber());
    }

    /**
     * Tests that the constructor creates a TrainDeparture
     * object with the given destination.
     */
    @Test
    @DisplayName("Should get destination when called")
    public void shouldGetDestinationWhenCalled() {
      Assertions.assertEquals("Oslo", departure.getDestination());
    }

    /**
     * Tests that the constructor creates a TrainDeparture
     * object with the given track.
     */
    @Test
    @DisplayName("Should get track when called")
    public void shouldGetTrackWhenCalled() {
      Assertions.assertEquals(1, departure.getTrack());
    }

    /**
     * Tests that the constructor creates a TrainDeparture
     * object with track set to -1 when no track parameter is given.
     */
    @Test
    @DisplayName("Should set track to -1 when not given as a parameter")
    public void shouldSetTrackToMinusOneWhenNotGivenAsParameter() {
      Assertions.assertEquals(-1, new TrainDeparture(
          LocalTime.parse("12:00"), 200, "F1", "Oslo").getTrack());
    }

    /**
     * Tests that the constructor creates a TrainDeparture
     * object with delay set to zero.
     */
    @Test
    @DisplayName("Should set delay to zero when departure is created")
    public void shouldSetDelayToZeroWhenDepartureIsCreated() {
      Assertions.assertEquals(Duration.ZERO, departure.getDelay());
    }
  }

  /**
   * Nested class that contains negative test related to the constructor.
   */
  @Nested
  @DisplayName("Negative tests for constructor")
  public class NegativeConstructorTest {

    /**
     * Tests that the constructor throws an IllegalArgumentException
     * when the given departure time is null.
     */
    @Test
    @DisplayName("Should not create TrainDeparture object when departure time is null")
    public void shouldThrowIllegalArgumentExceptionWhenDepartureTimeIsNull() {
      Assertions.assertThrows(IllegalArgumentException.class, () ->
          new TrainDeparture(null, 200, "F1", "Oslo", 1));
    }

    /**
     * Tests that the constructor throws an IllegalArgumentException
     * when the given line is null.
     */
    @Test
    @DisplayName("Should not create TrainDeparture object when the line is null")
    public void shouldThrowIllegalArgumentExceptionWhenLineIsNull() {
      Assertions.assertThrows(IllegalArgumentException.class, () ->
          new TrainDeparture(LocalTime.parse("12:00"), 200, null, "Oslo", 1));
    }

    /**
     * Tests that the constructor throws an IllegalArgumentException
     * when the given destination is null.
     */
    @Test
    @DisplayName("Should not create TrainDeparture object when destination is null")
    public void shouldThrowIllegalArgumentExceptionWhenDestinationIsNull() {
      Assertions.assertThrows(IllegalArgumentException.class, () ->
          new TrainDeparture(LocalTime.parse("12:00"), 200, "F1", null, 1));
    }

    /**
     * Tests that the constructor throws an IllegalArgumentException
     * when the given train number is less than 1.
     */
    @DisplayName("Should not create TrainDeparture object when train number is smaller than 1")
    @ParameterizedTest
    @ValueSource(ints = {-13, -40, 0})
    public void shouldThrowIllegalArgumentExceptionWhenTrainNumberIsInvalid(int trainNumber) {
      Assertions.assertThrows(IllegalArgumentException.class, () ->
          new TrainDeparture(LocalTime.parse("12:00"), trainNumber, "F1", "Oslo", 1));
    }
  }

  /**
   * Nested class that contains positive tests related to assigning track to departure.
   */
  @Nested
  @DisplayName("Tests related to assigning track to a departure")
  public class TrackTest {

    /**
     * Tests that the method setTrack sets the track to the given
     * track number when the given track number is greater than zero.
     */
    @Test
    @DisplayName("Assigning new track: Should set track when assigned track is greater than zero")
    public void shouldSetTrackWhenGreaterThanZero() {
      departure.setTrack(3);
      Assertions.assertEquals(3, departure.getTrack());
    }

    /**
     * Tests that the method setTrack does not set track if the given track is less than -1.
     */
    @Test
    @DisplayName("Assigning new track: Should not assign new track if track is less than -1")
    public void shouldThrowIllegalArgumentExceptionWhenTrackIsInvalid() {
      Assertions.assertThrows(IllegalArgumentException.class, () ->
          departure.setTrack(-3));
    }

    /**
     * Tests that the method setTrack sets the track to -1 when
     * the track is not assigned.
     */
    @DisplayName("Assigning new track: Should set track to -1 when not assigned")
    @ParameterizedTest
    @ValueSource(ints = {-1, 0})
    public void shouldSetTrackWhenNotAssigned(int track) {
      departure.setTrack(track);
      Assertions.assertEquals(-1, departure.getTrack());
    }
  }

  /**
   * Nested class that contains positive tests related to delay.
   */
  @Nested
  @DisplayName("Tests related to delay")
  public class DelayTest {

    /**
     * Tests that the method setDelay sets the delay to the given
     * delay, when the given delay is not negative or null.
     */
    @Test
    @DisplayName("Setting delay: Should set delay when not negative or null")
    public void shouldSetDelayWhenNotNegativeOrNull() {
      departure.setDelay(Duration.ofHours(1));
      Assertions.assertEquals(Duration.ofHours(1), departure.getDelay());
    }

    /**
     * Tests that the method getDelay returns the delay of the departure.
     */
    @Test
    @DisplayName("Should get delay when called")
    public void shouldGetDelayWhenCalled() {
      departure.setDelay(Duration.between(LocalTime.MIN, LocalTime.parse("00:10")));
      Assertions.assertEquals(Duration.between(LocalTime.MIN,
          LocalTime.parse("00:10")), departure.getDelay());
    }

    /**
     * Tests that the method setDelay does not set the delay if the given
     * delay is negative.
     */
    @Test
    @DisplayName("Setting delay: Should not set delay if delay is negative")
    public void testSetDelayShouldThrowIllegalArgumentExceptionWhenDelayIsNegative() {
      Assertions.assertThrows(IllegalArgumentException.class, () ->
          departure.setDelay(Duration.ofHours(-3)));
    }

    /**
     * Tests that the method setDelay does not set the delay if the given
     * delay is null.
     */
    @Test
    @DisplayName("Setting delay: Should not set delay if delay is null")
    public void testSetDelayShouldThrowIllegalArgumentExceptionWhenDelayIsNull() {
      Assertions.assertThrows(IllegalArgumentException.class, () ->
          departure.setDelay(null));
    }
  }

  /**
   * Nested class that contains positive tests related to comparing departure time.
   */
  @Nested
  @DisplayName("Tests related to departure time")
  public class CompareToTests {

    /**
     * Tests that the method compareTo returns 1 when the departure time is before the other.
     */
    @Test
    @DisplayName("CompareTo: Should return 1 when departure time is before other")
    public void shouldReturnOneWhenComparedToLaterDepartureTime() {
      TrainDeparture departureB = new TrainDeparture(
          LocalTime.parse("10:00"), 200, "F1", "Oslo");
      Assertions.assertEquals(1, departure.compareTo(departureB));
    }

    /**
     * Tests that the method compareTo returns 0 when the departure time is the same as the other.
     */
    @Test
    @DisplayName("CompareTo: Should return 0 when departure time is same as other")
    public void shouldReturnZeroWhenComparedToSameDepartureTime() {
      TrainDeparture departureB = new TrainDeparture(
          LocalTime.parse("12:00"), 200, "F1", "Oslo");
      Assertions.assertEquals(0, departure.compareTo(departureB));
    }

    /**
     * Tests that the method compareTo returns -1 when the departure time is after the other.
     */
    @Test
    @DisplayName("CompareTo: Should return -1 when departure time is after other")
    public void shouldReturnNegativeOneWhenComparedToEarlierDepartureTime() {
      TrainDeparture departureB = new TrainDeparture(
          LocalTime.parse("13:00"), 200, "F1", "Oslo");
      Assertions.assertEquals(-1, departure.compareTo(departureB));
    }
  }

}
